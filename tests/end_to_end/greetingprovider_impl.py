from typing import Any, Dict

from sila2.framework.feature import Feature
from sila2.framework.fully_qualified_identifier import FullyQualifiedIdentifier
from sila2.server.feature_implementation_base import FeatureImplementationBase
from tests.utils import get_feature_definition_str

GreetingProvider = Feature(get_feature_definition_str("GreetingProvider"))


class GreetingProviderImpl(FeatureImplementationBase):
    def SayHello(self, Name: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> str:
        return f"Hello {Name}"

    def get_StartYear(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> int:
        return 2021
