import time
import uuid

import pytest

from sila2.discovery.browser import SilaDiscoveryBrowser
from sila2.server.sila_server import SilaServer
from tests.utils import generate_port


def test():
    server_uuid = uuid.uuid4()

    class TestServer(SilaServer):
        def __init__(self):
            super().__init__(
                server_name="TestServer",
                server_type="TestServer",
                server_uuid=server_uuid,
                server_version="0.1",
                server_description="A test SiLA2 server",
                server_vendor_url="https://gitlab.com/sila2/sila_python",
            )

    server = TestServer()
    browser = SilaDiscoveryBrowser()

    with pytest.raises(TimeoutError):
        browser.find_server(timeout=0.1)

    try:
        port = generate_port()
        address = "127.0.0.1"
        server.start_insecure(address, port)  # discovery should be enabled by default

        with pytest.raises(ValueError):
            browser.find_server(timeout=-1)

        with pytest.raises(TimeoutError):
            browser.find_server(server_name="UnknownServer", timeout=0.1)
        with pytest.raises(TimeoutError):
            browser.find_server(server_uuid=uuid.uuid4(), timeout=0.1)

        for kwargs in [
            dict(),
            dict(server_name="TestServer"),
            dict(server_uuid=server_uuid),
            dict(server_uuid=server_uuid, server_name="TestServer"),
        ]:
            client = browser.find_server(**kwargs, timeout=10)

            assert client.SiLAService.ServerName.get() == server.server_name
            assert client.SiLAService.ServerUUID.get() == str(server_uuid)

    finally:
        server.stop()

    time.sleep(1)  # browser must first handle the unregistration request sent on server stop
    with pytest.raises(TimeoutError):
        browser.find_server(server_name="TestServer", timeout=0.5)
