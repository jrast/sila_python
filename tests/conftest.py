from types import ModuleType
from typing import Type

import pytest

from sila2.framework.feature import Feature
from tests.utils import get_feature_definition_str


@pytest.fixture
def basic_feature() -> Feature:
    return Feature(get_feature_definition_str("BasicDataTypes"))


@pytest.fixture
def silaframework_pb2_module(basic_feature) -> Type[ModuleType]:
    return basic_feature._pb2_module.SiLAFramework__pb2
