import functools
import os

import pytest
from sila2_example_server import Server

from sila2.framework.utils import temporarily_add_to_path


def run_example_client_script(func):
    @functools.wraps(func)
    @pytest.mark.filterwarnings("ignore")
    def wrapper(*args, **kwargs):
        server = Server()
        try:
            server.start_insecure("127.0.0.1", 50052, enable_discovery=True)
            with temporarily_add_to_path(os.path.join(os.path.dirname(__file__), "..", "example_client_scripts")):
                return func(*args, **kwargs)
        finally:
            server.stop()

    return wrapper


@run_example_client_script
def test_discovery(capsys):
    import discovery

    discovery.main()
    # print("AAA", capsys.readouterr(), "BBB")
    out = capsys.readouterr().out
    assert "Failed" not in out
    assert "SiLAService" in out
    assert "DelayProvider" in out
    assert "GreetingProvider" in out
    assert "TimerProvider" in out


@run_example_client_script
def test_error_handling():
    import error_handling

    error_handling.main()


@run_example_client_script
def test_metadata():
    import metadata

    metadata.main()


@run_example_client_script
def test_observable_command():
    import observable_command

    observable_command.main()


@run_example_client_script
def test_observable_property():
    import observable_property

    observable_property.main()


@run_example_client_script
def test_unobservables():
    import unobservables

    unobservables.main()
