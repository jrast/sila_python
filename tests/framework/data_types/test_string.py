import pytest


def test_to_native_type(basic_feature):
    string_field = basic_feature._data_type_definitions["Str"].data_type
    SilaString = string_field.message_type

    msg = SilaString(value="abc")
    val = string_field.to_native_type(msg)

    assert isinstance(val, str)
    assert val == "abc"


def test_to_message(basic_feature):
    string_field = basic_feature._data_type_definitions["Str"].data_type
    SilaString = string_field.message_type

    msg = string_field.to_message("def")

    assert isinstance(msg, SilaString)
    assert msg.value == "def"


def test_wrong_type(basic_feature):
    string_field = basic_feature._data_type_definitions["Str"].data_type

    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        string_field.to_message(b"abc")

    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        string_field.to_message(123)


def test_too_long(basic_feature):
    string_field = basic_feature._data_type_definitions["Str"].data_type

    longest_allowed_string = "a" * 2 ** 21

    _ = string_field.to_message(longest_allowed_string)
    with pytest.raises(ValueError):
        string_field.to_message("a" + longest_allowed_string)


def test_from_string(basic_feature):
    string_field = basic_feature._data_type_definitions["Str"].data_type

    assert string_field.from_string("abc") == "abc"
