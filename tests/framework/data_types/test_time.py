from datetime import time, timedelta, timezone

import pytest


def test_to_native_type(basic_feature):
    time_field = basic_feature._data_type_definitions["Time"].data_type
    SilaTime = time_field.message_type
    SilaTimezone = basic_feature._pb2_module.SiLAFramework__pb2.Timezone

    msg = SilaTime(hour=1, minute=2, second=3, timezone=SilaTimezone(hours=3, minutes=30))
    t = time_field.to_native_type(msg)

    assert isinstance(t, time)
    assert t.hour == 1
    assert t.minute == 2
    assert t.second == 3
    assert t.tzinfo.utcoffset(None) == timedelta(hours=3, minutes=30)


def test_to_message(basic_feature):
    time_field = basic_feature._data_type_definitions["Time"].data_type
    SilaTime = time_field.message_type

    msg = time_field.to_message(time(hour=1, minute=2, second=3, tzinfo=timezone(timedelta(hours=3, minutes=30))))

    assert isinstance(msg, SilaTime)
    assert msg.hour == 1
    assert msg.minute == 2
    assert msg.second == 3
    assert msg.timezone.hours == 3
    assert msg.timezone.minutes == 30


def test_wrong_type(basic_feature):
    time_field = basic_feature._data_type_definitions["Time"].data_type

    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        time_field.to_message(1)


def test_seconds_in_timezone(basic_feature):
    time_field = basic_feature._data_type_definitions["Time"].data_type

    with pytest.raises(ValueError):
        _ = time_field.to_message(
            time(
                hour=1,
                minute=2,
                second=3,
                tzinfo=timezone(timedelta(hours=3, minutes=30, seconds=1)),
            )
        )


def test_missing_timezone(basic_feature):
    time_field = basic_feature._data_type_definitions["Time"].data_type

    with pytest.raises(ValueError):
        _ = time_field.to_message(time(hour=1, minute=2, second=3))


def test_from_string(basic_feature):
    time_field = basic_feature._data_type_definitions["Time"].data_type

    assert time_field.from_string("23:59:00Z") == time(23, 59, tzinfo=timezone(timedelta(0)))
    assert time_field.from_string("23:59:13-02:30") == time(
        23, 59, 13, tzinfo=timezone(timedelta(hours=-2, minutes=-30))
    )
