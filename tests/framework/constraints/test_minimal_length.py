import pytest

from sila2.framework.constraints.minimal_length import MinimalLength


def test():
    length3 = MinimalLength(3)
    assert length3.validate("abc")
    assert length3.validate(b"abc")
    assert length3.validate("abcd")
    assert not length3.validate("ab")

    assert repr(length3) == "MinimalLength(3)"

    _ = MinimalLength(2 ** 63 - 1)
    with pytest.raises(ValueError):
        _ = MinimalLength(-1)
    with pytest.raises(ValueError):
        _ = MinimalLength(2 ** 63)
