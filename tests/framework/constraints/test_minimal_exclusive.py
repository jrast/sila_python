from sila2.framework.constraints.minimal_exclusive import MinimalExclusive
from sila2.framework.data_types.integer import Integer
from sila2.framework.data_types.real import Real


def test(silaframework_pb2_module):
    m_int = MinimalExclusive("3", Integer(silaframework_pb2_module))
    assert not m_int.validate(2)
    assert not m_int.validate(3)
    assert m_int.validate(4)

    assert repr(m_int) == "MinimalExclusive(3.0)"

    m_float = MinimalExclusive("2.5", Real(silaframework_pb2_module))
    assert not m_float.validate(2.4)
    assert not m_float.validate(2.5)
    assert m_float.validate(3)


def test_scientific_notation(silaframework_pb2_module):
    m_int = MinimalExclusive("3e5", base_type=Integer(silaframework_pb2_module))
    m_float = MinimalExclusive("1.23e-4", base_type=Real(silaframework_pb2_module))

    assert not m_int.validate(300_000)
    assert m_int.validate(300_001)

    assert not m_float.validate(0.000123)
    assert m_float.validate(0.0001231)
