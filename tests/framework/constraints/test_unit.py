from sila2.framework.constraints.unit import SIUnit, Unit, UnitComponent


def test():
    hertz = Unit("Hz", 1, 0, [UnitComponent(SIUnit.Second, -1)])
    assert hertz.validate()  # always True
    assert repr(hertz) == "Unit('Hz': 's^-1')"

    celsius = Unit("°C", 1, -273.15, [UnitComponent(SIUnit.Kelvin, 1)])
    assert repr(celsius) == "Unit('°C': 'K + -273.15')"

    fahrenheit = Unit("°F", 0.555, 255.372, [UnitComponent(SIUnit.Kelvin, 1)])
    assert repr(fahrenheit) == "Unit('°F': 'K * 0.555 + 255.372')"

    joule = Unit(
        "J", 1, 0, [UnitComponent(SIUnit.Kilogram, 1), UnitComponent(SIUnit.Meter, 2), UnitComponent(SIUnit.Second, -2)]
    )
    assert repr(joule) == "Unit('J': 'kg * m^2 * s^-2')"
