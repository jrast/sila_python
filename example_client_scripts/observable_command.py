import time

from sila2_example_server import Client
from sila2_example_server.generated.timerprovider import CountdownTooLong

from sila2.framework import CommandExecutionStatus


def main():
    client = Client("127.0.0.1", 50052)

    # calling an observable command returns a ClientObservableCommandInstance
    seconds = 5
    print(f"Starting {seconds} second countdown")
    countdown_instance = client.TimerProvider.Countdown(N=seconds, Message="Countdown ended")

    # the ClientObservableCommandInstance subscribes to the execution information and updates itself accordingly
    # until the first execution information is received, the values are `None`
    # - status: CommandExecutionStatus (Enum)
    # - progress: float (0 to 100)
    # - estimated_remaining_time: datetime.timedelta (time since last update is subtracted on attribute access)
    # - lifetime_of_execution: datetime.timedelta (time since last update is subtracted on attribute access)
    # - done: bool (False until status is `finishedSuccessfully` or `finishedWithError`)

    # subscribe to the intermediate responses (optional)
    intermediate_responses = countdown_instance.subscribe_intermediate_responses()

    # intermediate response streams are iterable
    for intermediate_response in intermediate_responses:
        # add a short delay to ensure the execution info is updated properly
        time.sleep(0.1)

        # like command responses, intermediate responses are returned as NamedTuples
        print(
            f"  Current number: {intermediate_response.CurrentNumber}, "
            f"Status: {countdown_instance.status}, "
            f"Progress: {countdown_instance.progress}, "
            f"Remaining time: {countdown_instance.estimated_remaining_time}"
        )

    # when the execution is done, the results can be queried
    message, timestamp = countdown_instance.get_responses()
    print(f"  {timestamp.isoformat()}: {message}")

    # intermediate response streams can be cancelled:
    print("Starting another countdown")
    countdown_instance = client.TimerProvider.Countdown(N=5, Message="Countdown ended")
    intermediate_responses = countdown_instance.subscribe_intermediate_responses()
    for intermediate_response in intermediate_responses:
        num = intermediate_response.CurrentNumber
        print(f"  Current number: {num}, Remaining: {countdown_instance.estimated_remaining_time}")
        if num == 2:
            intermediate_responses.cancel()
            print("  Cancelled subscription")

    while not countdown_instance.done:
        print(f"  Remaining: {countdown_instance.estimated_remaining_time}")
        time.sleep(0.5)

    message, timestamp = countdown_instance.get_responses()
    print(f"  {timestamp.isoformat()}: {message}")

    # errors are raised when requesting the result
    print("Starting countdown with invalid duration")
    countdown_instance = client.TimerProvider.Countdown(N=9001, Message="This will never end")
    time.sleep(0.5)
    assert countdown_instance.status == CommandExecutionStatus.finishedWithError
    try:
        countdown_instance.get_responses()
    except CountdownTooLong:
        print("Countdown duration was invalid and the error was caught")


if __name__ == "__main__":
    main()
