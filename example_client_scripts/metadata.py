from sila2_example_server import Client

from sila2.framework import InvalidMetadata


def main():
    client = Client("127.0.0.1", 50052)

    # SiLA Client Metadata can be sent to SiLA Servers attached to command invocation and property access/subscription.
    # Metadata can be specified as keyword-only argument and, if given, must be a dictionary.
    # The dictionary keys must be the metadata objects, or their fully qualified identifiers.

    # In this example, the Delay metadata of the DelayProvider feature is used to delay execution by N milliseconds.
    # By default, DelayProvider is inactive and does not affect any calls:

    # Deactivate
    client.DelayProvider.Deactivate()

    print("DelayProvider active:", client.DelayProvider.IsActive.get())
    print("Affected calls:", client.DelayProvider.Delay.get_affected_calls())
    print("StartYear without delay:", client.GreetingProvider.StartYear.get())

    # Now we activate the DelayProvider
    print("Activate DelayProvider")
    client.DelayProvider.Activate()

    print("DelayProvider active:", client.DelayProvider.IsActive.get())
    print("Affected calls:")
    for identifier in client.DelayProvider.Delay.get_affected_calls():
        print("-", identifier)

    # Metadata is sent as a dictionary
    print("Calling with delay:")
    print("StartYear with delay:", client.GreetingProvider.StartYear.get(metadata=[client.DelayProvider.Delay(1000)]))

    # When required metadata is not sent, a InvalidMetadata error is raised by the server
    try:
        client.GreetingProvider.StartYear.get()
    except InvalidMetadata as ex:
        print("Caught InvalidMetadata error:", ex)


if __name__ == "__main__":
    main()
