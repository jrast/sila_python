from sila2_example_server import Client

from sila2.discovery import SilaDiscoveryBrowser


def main():
    try:
        print("Starting SiLA Discovery...")
        client: Client = SilaDiscoveryBrowser().find_server(server_name="ExampleServer", timeout=15)

        print("Discovered SiLA Server with the following features:")
        for feature_identifier in client.SiLAService.ImplementedFeatures.get():
            print("-", feature_identifier)

    except TimeoutError:
        print("Failed to discover SiLA Server")


if __name__ == "__main__":
    main()
