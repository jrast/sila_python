from __future__ import annotations

from sila2.client import ClientMetadata

class AuthorizationServiceClient:
    """
    This Feature provides access control for the implementing server.

    It specifies the SiLA Client Metadata for the access token, that has been provided by the
    AuthenticationService core Feature.
    """

    AccessToken: ClientMetadata
    """
    Token to be sent with every call in order to get access to the SiLA Server functionality.
    """
