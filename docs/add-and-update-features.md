# Add and update features
During the development cycle of SiLA server applications, it can happen that you want to add new SiLA features or modify already implemented ones.

**NOTE:** If you are using sila2 version 0.3.5 or older the `sila2-codegen` script is not available.
Replace all `sila2-codegen` calls with `python -m sila2.code_generator`.

## Add new features
To add new features, use the `add-features` code generation command:
```shell
$ sila2-codegen add-features [-d package-directory] NewFeature1.sila.xml NewFeature2.sila.xml ...
```

Let's start by generating a new SiLA server package `my_sila_package` with no custom features.
```shell
$ sila2-codegen new-package -n my_sila_package
```

### Run code generator
Like in the basic example, we add the [GreetingProvider](https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/examples/GreetingProvider.sila.xml) feature.
```shell
$ sila2-codegen add-features path/to/GreetingProvider.sila.xml
```

### Implement feature
This has generated all relevant files, including `my_sila_package/feature_implementations/greetingprovider_impl.py`, where we now have to implement the commands and properties.
Open the file and replace both lines `raise NotImplementedError`.
- `SayHello`: `return f"Hallo {Name}"`
- `get_StartYear`: `return 2021`

### Add feature to server
In contrast to `new-package`, which adds features to the server automatically, with `add-features` you have to do that manually.
Open the file `my_sila_package/server.py`.

First, you need to import the generated feature and your implementation class:
- `from .generated.greetingprovider import GreetingProviderFeature`
- `from .feature_implementations.greetingprovider_impl import GreetingProviderImpl`

The feature object (`GreetingProviderFeature` in this case) reflects the content of the feature definition file (`GreetingProvider.sila.xml`) and is required to let the server know about the functionality your implementation provides.

To add your feature implementation to the server, `self.set_feature_implementation(feature, implementation)` needs to be called in the `__init__` method of the server class.

First, create an instance of your feature implementation class:
- `self.greetingprovider_impl = GreetingProviderImpl()`

Then tell the server that this object should implement the feature:
- `self.set_feature_implementation(GreetingProviderFeature, self.greetingprovider_impl)`

If you don't need the reference, you can also do this in one line:
- `self.set_feature_implementation(GreetingProviderFeature, GreetingProviderImpl())`

The result should look like this:
```python
class Server(SilaServer):
    def __init__(self):
        super().__init__(...)

        ...  # code for other features
        self.greetingprovider = GreetingProviderImpl()
        self.set_feature_implementation(GreetingProviderFeature, self.greetingprovider)
```

## Update feature
If you want to change features that already are part of a server package, use the `update` code generation command:
```shell
$ sila2-codegen update [-d package-directory]
```

### Change feature definition
For this example, let's modify the GreetingProvider feature by changing the command `SayHello(Name)` to `SayHelloWorld()`.
Open the file `my_sila_package/generated/greetingprovider/GreetingProvider.sila.xml` and change the `<Command>` element:
- change the `<Identifier>` from `SayHello` to `SayHelloWorld`
- change the `<DisplayName>` from `Say Hello` to `Say Hello World`
- change the `<Description>` to `Returns "Hello World"`
- remove the `<Parameter>` element

### Update the package
Run the following command:
```shell
$ sila2-codegen update
```

This regenerates the content of the directory `my_sila_package/generated` to match the contained feature definitions.

Additionally, it places new empty feature implementation files in the directory `my_sila_package/feature_implementations`, all with the prefix `updated_`.
To finish the update, merge the old and new implementation. In the end, the file `greetingprovider_impl.py` should contain a class `GreetingProviderImpl` with all method signatures like in `updated_greetingprovider_impl.py`.

In this case:
- copy `return 2021` from the original implementation to the updated file
- add `return "Hello World"` to the `SayHelloWorld` method in the updated file
- rename `updated_greetingprovider_impl.py` to `greetingprovider_impl.py` so it replaces the old implementation
