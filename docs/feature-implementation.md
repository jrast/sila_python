# Details of SiLA Feature Implementation
The tutorial skips the SiLA server generation and instead focuses on the implementation of features.
For instructions on how to use the code generator to auto-generate the base classes, see the server implementation guide.

## Examples
If you need examples, please have a look at the example server in this repository.

## Feature implementation base class
The code generator generates an [abstract base class](https://docs.python.org/3/library/abc.html) with the suffix `Base` from a feature definition file, e.g. `GreetingProviderBase`.

To implement a feature, inherit from this base class and implement its abstract methods. The code generator generates these implementation classes for you in the `feature_implementation` directory, e.g. `GreetingProviderImpl`.

### `start()` and `stop()`
Feature implementations inherit two methods from the class `sila2.server.FeatureImplementationBase`: `start(self)` and `stop(self)`.
When the server is started, it calls the `start` method of all registered implementations. When it shuts down, it calls the `stop` method of all registered implementations.

By default, these methods are empty. You can use them e.g. to open and close connections, or to start and stop background threads.

### Unobservable properties
For each unobservable property `MyProperty`, the base class defines one abstract method `get_MyProperty(self, *, metadata)`.
It is called when a SiLA client requests the value of that property.

Example: [`GreetingProvider.StartYear`](../example_server/sila2_example_server/feature_implementations/greetingprovider_impl.py)

### Unobservable commands
For each unobservable command `MyCommand`, the base class defines one abstract method `MyCommand(self, Param1, Param2, ..., *, metadata)`.
It is called when a SiLA client requests the execution of that command.

Example: [`GreetingProvider.SayHello`](../example_server/sila2_example_server/feature_implementations/greetingprovider_impl.py)

### Observable properties
For each observable property `MyProperty`, the base class defines two (non-abstract) methods:
- `MyProperty_on_subscription(self, *, metadata)`: Called when a SiLA client requests to subscribe to the property. It is empty by default. You can override it, e.g. to raise errors or start background threads.
- `update_MyProperty(self, value)`: You can call this method to send a value to all current subscribers of `MyProperty`.

By default, the last value is cached and immediately sent to new subscribers.

One possible way to implement observable properties is to call `self.update_MyProperty(value)` periodically from a background thread.

Example: [`TimerProvider.CurrentTime`](../example_server/sila2_example_server/feature_implementations/timerprovider_impl.py)

### Observable commands
For each observable command `MyCommand`, the base class defines one abstract method `MyCommand(self, Param1, Param2, ..., *, metadata, instance)`.

When a SiLA client requests the execution of `MyCommand`, the SiLA server executes the corresponding method in a separate thread and immediately responds with the command confirmation.

To update execution information and send intermediate responses, the server provides a `ObservableCommandInstance` object (the parameter `instance`):
- You can update the status, progress and estimated remaining time by setting `instance.status`, `instance.progress` and `instance.estimated_remaining_time`
- You can send intermediate responses by calling `instance.send_intermediate_response(intermediate_response_value)`

Example: [`TimerProvider.Countdown`](../example_server/sila2_example_server/feature_implementations/timerprovider_impl.py)

### Execution errors
#### Defined execution errors
The code generator not only creates the base class, but also one class for each defined execution error.
If a feature `ExampleFeature` defines an error `ExampleError`, you can import it with `from [...].generated.examplefeature import ExampleError`.

To raise this error, just use `raise ExampleError` to use the description from the feature definition as error message, or `raise ExampleError("your error message")` to send a custom message.

Example: [`TimerProvider.Countdown`](../example_server/sila2_example_server/feature_implementations/timerprovider_impl.py) raises `CountdownTooLong`.

#### Undefined execution errors
When any other exception occurs, it is sent to the client as an undefined execution error with the message `"ExceptionType: message"`.
You can raise custom undefined execution errors with `raise UndefinedExecutionError(message)` (requires `from sila2.framework import UndefinedExecutionError`)

### Metadata
#### Receiving metadata
The methods corresponding to command execution and property access have a parameter `metadata`, which is a dictionary.
When a SiLA client sends SiLA Client Metadata along with a request, that dictionary will have an entry for each metadata value received.

Dictionary keys are fully qualified metadata identifiers (string), the values are the received data (e.g. an integer or string).

#### Specifying calls affected by metadata
For each metadata definition `MyMetadata`, the base class defines an abstract method `get_calls_affected_by_MyMetadata(self)`.

It is expected to return a list of command, property, and feature objects (or fully qualified identifiers as plain strings).

## Internals
### Fully qualified identifiers
The SiLA 2 standard specifies that fully qualified identifiers should be compared in a case-insensitive manner.
The class `sila2.framework.FullyQualifiedIdentifier` inherits from the built-in type `str` and overrides the equality operators to ensure this behavior. 

### Data type conversion ("Where are the protobuf messages?")
When a SiLA server receives a request from a SiLA client, it unpacks the received protobuf message into Python objects that are then sent to the feature implementations.
When the feature implementations hand values back to the server (as return value, via a queue or on observable property updates), the values are unpacked into the appropriate protobuf message and then sent to the client.

### Constraints
When a SiLA server unpacks received parameters of a constrained data type, all constraints are checked before invoking a method of the requested feature implementation.
If a constraint is not satisfied, the SiLA server responds with a `ValidationError`.

### Calls not affected by metadata
When a SiLA client sends metadata with a call that should not be affected by that metadata, that metadata is not handed over to the feature implementation.

### Binary transfer
When sending or receiving binary objects larger than 2 MB, SiLA Binary Transfer is used to transfer the data. How data is transferred is not visible to the feature implementation.
