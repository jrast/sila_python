# SiLA Server Implementation
This document guides you through the process of implementing a basic SiLA server with the example feature [GreetingProvider](https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/examples/GreetingProvider.sila.xml).

## Preparation
### Requirements
This tutorial requires Python version 3.7 or higher, as well as the package manager Pip (usually included in Python installations).
It is assumed that the Python and Pip programs are available as `python` and `pip`.
On some systems, you might need to use `python3` and `python3 -m pip` instead.

### Feature definitions
The capabilities of a SiLA server are specified by the features it implements.
These features are defined in feature definition files (file extension `.sila.xml`).

This example starts with the [GreetingProvider](https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/examples/GreetingProvider.sila.xml) feature.
Download the file `GreetingProvider.sila.xml` and place it somewhere where you can find it in the next step.

## Package generation
**NOTE:** If you are using sila2 version 0.3.5 or older the `sila2-codegen` script is not available.
Replace all `sila2-codegen` calls with `python -m sila2.code_generator`.

This library comes with a code generation tool that generates a Python package from feature definitions. It can be used as follows:
```shell
sila2-codegen new-package -n [package-name] [-o output-directory] [feature-definition-files]
```

Open a terminal and execute the following command (adapting the path to the feature definition):
```shell
sila2-codegen new-package -n example_server -o example_server path/to/GreetingProvider.sila.xml
```

This generates a directory `example_server`, containing a full SiLA 2 server application missing only the feature implementation.
You can safely remove the original feature definition file, as it is copied into the generated package.

### Generated code
You can skip this section. It explains the role of all generated files. The important parts are covered in the next sections.

The previous command has generated the following file structure:
```shell
.
└── example_server  # application directory, option '-o', installable with 'pip install .'
    ├── example_server  # package directory, option '-n'
    │   ├── generated  # required files for interacting with the server, don't touch these files
    │   │   ├── greetingprovider  # files for the GreetingProvider feature
    │   │   │   ├── __init__.py
    │   │   │   ├── GreetingProvider.sila.xml  # copy of the feature definition
    │   │   │   ├── greetingprovider_base.py  # abstract base class for feature implementations
    │   │   │   ├── greetingprovider_client.pyi  # stub file for client-side interactions
    │   │   │   ├── greetingprovider_errors.py  # defined execution errors
    │   │   │   ├── greetingprovider_feature.py  # feature object
    │   │   │   └── greetingprovider_types.py  # type definitions
    │   │   ├── __init__.py
    │   │   └── client.py  # SiLA client class for interacting with the server 
    │   ├── feature_implementations  # feature implementations, add your logic here
    │   │   ├── __init__.py
    │   │   └── greetingprovider_impl.py  # feature implementation of GreetingProvider
    │   ├── __init__.py
    │   ├── __main__.py  # makes module executable
    │   └── server.py  # SiLA server class, adds feature implementations
    ├── MANIFEST.in
    └── setup.py  # makes module installable
```

## Server information
Every SiLA server must provide a name, type, version, description and vendor url. Open the file `example_server/example_server/server.py` and change these fields:

```python
super().__init__(
    server_name="MyFirstServer",
    server_type="Example",
    server_version="0.1",
    server_description="A basic SiLA 2 server",
    server_vendor_url="https://gitlab.com/SiLA2/sila_python",
)
```

## Feature implementation
To implement the GreetingProvider feature, open the file `example_server/example_server/feature_implementations/greetingprovider_impl.py`.

It defines a class `GreetingProviderImpl`, which inherits from the auto-generated class `GreetingProviderBase`.
You only need to change the methods that currently raise a `NotImplementedError`.

Replace the body of the method `SayHello` with `return f"Hello SiLA 2 {Name}""`, and the body of `get_StartYear` with `return 2022`.

## Installation
You can now install the server application with `pip install [path to directory containing setup.py]`, e.g. `pip install example_server/`.

## Run the server
The server can then be started with `python -m example_server`. This will start a SiLA 2 server on IP address 127.0.0.1 and port 50052.
Set the option `--help` to see available settings.
