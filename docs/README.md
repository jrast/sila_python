# Documentation
- [Implement your first SiLA 2 server](server-implementation.md)
- [Use SiLA 2 clients to communicate with servers](client-usage.md)
- [Implement more complex features](feature-implementation.md)
- [Add Locking and Auth features](locking-and-auth.md)
- [Add and update features](add-and-update-features.md)
- [How SiLA data types are mapped to Python types](data-types.md)
