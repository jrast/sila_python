from __future__ import annotations

from typing import Iterable, Optional

from delayprovider_types import Activate_Responses, Deactivate_Responses

from sila2.client import ClientMetadata, ClientMetadataInstance, ClientUnobservableProperty

class DelayProviderClient:
    """
    Allows adding delay to calls via SiLA Client Metadata
    """

    IsActive: ClientUnobservableProperty[bool]
    """
    If active, some calls to the server will require the Delay metadata.
    """

    Delay: ClientMetadata[int]
    """
    When receiving this metadata in the context of a call, the server should wait for the specified duration
      before staring the requested execution
    """
    def Activate(self, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None) -> Activate_Responses:
        """
        Activate this feature.
        """
        ...
    def Deactivate(self, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None) -> Deactivate_Responses:
        """
        Deactivate this feature.
        """
        ...
