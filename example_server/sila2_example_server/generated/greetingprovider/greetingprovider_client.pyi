from __future__ import annotations

from typing import Iterable, Optional

from greetingprovider_types import SayHello_Responses

from sila2.client import ClientMetadataInstance, ClientUnobservableProperty

class GreetingProviderClient:
    """
    Example implementation of a minimum Feature. Provides a Greeting to the Client
    and a StartYear property, informing about the year the Server has been started.
    """

    StartYear: ClientUnobservableProperty[int]
    """
    Returns the year the SiLA Server has been started in.
    """
    def SayHello(self, Name: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None) -> SayHello_Responses:
        """
        Does what it says: returns "Hello SiLA 2 + [Name]" to the client.
        """
        ...
