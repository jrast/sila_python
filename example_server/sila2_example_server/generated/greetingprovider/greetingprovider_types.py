from __future__ import annotations

from typing import NamedTuple


class SayHello_Responses(NamedTuple):

    Greeting: str
    """
    The greeting string, returned to the SiLA Client.
    """
