from os.path import dirname, join

from sila2.framework import Feature

GreetingProviderFeature = Feature(open(join(dirname(__file__), "GreetingProvider.sila.xml")).read())
