from os.path import dirname, join

from sila2.framework import Feature

TimerProviderFeature = Feature(open(join(dirname(__file__), "TimerProvider.sila.xml")).read())
