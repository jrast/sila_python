from __future__ import annotations

from abc import ABC, abstractmethod
from datetime import time
from queue import Queue
from typing import Any, Dict

from sila2.framework import FullyQualifiedIdentifier
from sila2.server import FeatureImplementationBase, ObservableCommandInstanceWithIntermediateResponses

from .timerprovider_types import Countdown_IntermediateResponses, Countdown_Responses


class TimerProviderBase(FeatureImplementationBase, ABC):

    _CurrentTime_producer_queue: Queue[time]

    def __init__(self):
        """
        Provides a timer command and the current time
        """

        self._CurrentTime_producer_queue = Queue()

    def update_CurrentTime(self, CurrentTime: time):
        """
        The current time

        This method updates the observable property 'CurrentTime'.
        """
        self._CurrentTime_producer_queue.put(CurrentTime)

    def CurrentTime_on_subscription(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> None:
        """
        The current time

        This method is called when a client subscribes to the observable property 'CurrentTime'

        :param metadata: The SiLA Client Metadata attached to the call
        :return:
        """
        pass

    @abstractmethod
    def Countdown(
        self,
        N: int,
        Message: str,
        *,
        metadata: Dict[FullyQualifiedIdentifier, Any],
        instance: ObservableCommandInstanceWithIntermediateResponses[Countdown_IntermediateResponses],
    ) -> Countdown_Responses:
        """
        Count down from N to 0, then return the given message and the current time


        :param N: The number from which to count down

        :param Message: The message to return on completion

        :param metadata: The SiLA Client Metadata attached to the call
        :param instance: The command instance, enabling sending status updates to subscribed clients

        :return:

            - Message: The message provided as parameter

            - Timestamp: The timestamp when the countdown finished


        """
        pass
