from __future__ import annotations

from datetime import time
from typing import Iterable, Optional

from timerprovider_types import Countdown_IntermediateResponses, Countdown_Responses

from sila2.client import ClientMetadataInstance, ClientObservableCommandInstance, ClientObservableProperty

class TimerProviderClient:
    """
    Provides a timer command and the current time
    """

    CurrentTime: ClientObservableProperty[time]
    """
    The current time
    """
    def Countdown(
        self, N: int, Message: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> ClientObservableCommandInstance[Countdown_IntermediateResponses, Countdown_Responses]:
        """
        Count down from N to 0, then return the given message and the current time
        """
        ...
