from __future__ import annotations

from typing import Any, NamedTuple


class ComplexCommand_Responses(NamedTuple):

    StructureType: StructureType
    """
    The structure type
    """

    InlineStructure: Any
    """
    A structure defined in the response element
    """


IntegerAlias = int

StructureType = Any
