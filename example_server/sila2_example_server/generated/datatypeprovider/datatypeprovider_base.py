from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Dict

from sila2.framework import FullyQualifiedIdentifier
from sila2.server import FeatureImplementationBase

from .datatypeprovider_types import ComplexCommand_Responses, IntegerAlias, StructureType


class DataTypeProviderBase(FeatureImplementationBase, ABC):

    """
    Defines commands and properties to showcase handling of different data types
    """

    @abstractmethod
    def get_StructureProperty(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> Any:
        """
        A structure property

        :param metadata: The SiLA Client Metadata attached to the call
        :return: A structure property
        """
        pass

    @abstractmethod
    def ComplexCommand(
        self, Number: IntegerAlias, Structure: StructureType, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> ComplexCommand_Responses:
        """
        A command with complex data types


        :param Number: An aliased integer

        :param Structure: The structure type

        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - StructureType: The structure type

            - InlineStructure: A structure defined in the response element


        """
        pass
