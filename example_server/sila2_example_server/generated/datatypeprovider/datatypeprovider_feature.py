from os.path import dirname, join

from sila2.framework import Feature

DataTypeProviderFeature = Feature(open(join(dirname(__file__), "DataTypeProvider.sila.xml")).read())
