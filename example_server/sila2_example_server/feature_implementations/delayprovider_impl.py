from __future__ import annotations

import time
from typing import Any, Dict, List, Union

from sila2.framework import Command, Feature, FullyQualifiedIdentifier, Property
from sila2.server import MetadataInterceptor, SilaServer

from ..generated.delayprovider import (
    Activate_Responses,
    Deactivate_Responses,
    DelayProviderBase,
    DelayProviderFeature,
    DelayTooLong,
)


class DelayProviderInterceptor(MetadataInterceptor):
    delay_identifier: FullyQualifiedIdentifier = DelayProviderFeature["Delay"].fully_qualified_identifier
    max_delay_milliseconds: int = 10_000

    def __init__(self):
        self.delay_identifier = DelayProviderFeature["Delay"].fully_qualified_identifier
        super().__init__([self.delay_identifier])

    def intercept(
        self, parameters: Any, metadata: Dict[FullyQualifiedIdentifier, Any], target_call: FullyQualifiedIdentifier
    ) -> None:
        delay: int = metadata.pop(self.delay_identifier)
        if delay > self.max_delay_milliseconds:
            raise DelayTooLong(f"Maximum delay is {self.max_delay_milliseconds}, got {delay}")

        time.sleep(delay / 1000)


class DelayProviderImpl(DelayProviderBase):
    active: bool

    def __init__(self, parent_server: SilaServer):
        self.parent_server = parent_server
        self.parent_server.add_metadata_interceptor(DelayProviderInterceptor())
        self.active = False

    def get_IsActive(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> bool:
        return self.active

    def Activate(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> Activate_Responses:
        self.active = True

    def Deactivate(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> Deactivate_Responses:
        self.active = False

    def get_calls_affected_by_Delay(self) -> List[Union[Feature, Command, Property, FullyQualifiedIdentifier]]:
        if self.active:
            return [self.parent_server["GreetingProvider"]["StartYear"]]
        return []
