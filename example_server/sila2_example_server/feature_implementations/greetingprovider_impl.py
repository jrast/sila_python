import datetime
from typing import Any, Dict, Union

from sila2_example_server.generated.greetingprovider import GreetingProviderBase, SayHello_Responses

from sila2.framework import FullyQualifiedIdentifier


class GreetingProviderImpl(GreetingProviderBase):
    def SayHello(self, Name: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> Union[str, SayHello_Responses]:
        return f"Hello SiLA 2 {Name}"

    def get_StartYear(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> int:
        return datetime.datetime.now().year
